# Photo Lab
check it by yourself. OPEN [DEMO](http://lesyo.eu/gitLab/photoLab/about.html)
## It's a web application that can do:
- Open Albums (dynamically)
- Open Photos on Image Box (script for opening image)
- Rename Albums
- Delete Photos and Albums
- Has Images and Albums

## Technologies I use
- JavaScript
- jQuery
- React, opureReact
- EcmaScript
- Redux
- HTML
- CSS

## How to run

`cd ./photoLab
npm instal
npm start
`

## Read more
`/photoLab/public/about.html`
